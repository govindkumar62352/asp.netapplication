﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ASPWebApplication.DotNetFramWork.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .newStyle1 {
            color: #008000;
            font-style: oblique;
            font-family: "times New Roman", Times, serif;
            text-align: center;
            margin: 50px
        }
        .auto-style1 {
            width: 121px;
        }
        .auto-style2 {
            margin-left: 0px;
        }
        .auto-style3 {
            width: 145px;
        }
        .auto-style5 {
            width: 30%;
            height: 154px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="height: 665px">
            <h1><span class="newStyle1">Login Form</span></h1>
            <br />
            <asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" Text="Login" />
            <br />
            <br />
            <table class="auto-style5">
                <tr>
                    <td class="auto-style1">
            <asp:Label ID="Label1" runat="server" Text="UserName"></asp:Label>
                    </td>
                    <td class="auto-style3">
            <asp:TextBox ID="txtUserName" runat="server" style="margin-bottom: 0px"></asp:TextBox>
                    </td>
                  
                </tr>
                <tr>
                    <td class="auto-style1">
            <asp:Label ID="Label2" runat="server" Text="Password"></asp:Label>
                    </td>
                    <td class="auto-style3">
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                    
                </tr>
                <tr>
                    <td class="auto-style1">
            <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" />
                    </td>
                    <td class="auto-style3">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="auto-style2" />
                    </td>
                   
                </tr>
            </table>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
&nbsp;&nbsp;
            <br />
            <br />
&nbsp;&nbsp;
            &nbsp;
            <br />
&nbsp;&nbsp;&nbsp;
            <br />
&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </div>
    </form>
</body>
</html>
