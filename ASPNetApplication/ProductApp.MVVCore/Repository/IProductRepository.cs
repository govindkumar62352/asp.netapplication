﻿using ProductApp.MVVCore.Models;

namespace ProductApp.MVVCore.Repository
{
    public interface IProductRepository
    {
        List<Product> GetProducts();
        //bool AddProduct(Product product);
        public int AddProduct(Product product);
        Product? DeleteProductById(int id);
        // bool DeleteProduct(int id);
        public int DeleteProduct(int id);
        Product? GetProductById(int id);
        Product? GetProductByName(string? name);
    }
}
