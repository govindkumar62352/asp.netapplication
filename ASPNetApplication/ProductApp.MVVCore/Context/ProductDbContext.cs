﻿using Microsoft.EntityFrameworkCore;
using ProductApp.MVVCore.Models;

namespace ProductApp.MVVCore.Context
{
    public class ProductDbContext:DbContext
    {
        public ProductDbContext()
        {

        }
        public ProductDbContext(DbContextOptions<ProductDbContext> options) : base(options)
        {
           // Database.EnsureCreated();
        }

        public DbSet<Product> Products { get; set; }
    }
}