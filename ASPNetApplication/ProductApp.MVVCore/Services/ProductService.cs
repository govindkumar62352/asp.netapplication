﻿using ProductApp.MVVCore.Exceptions;
using ProductApp.MVVCore.Models;
using ProductApp.MVVCore.Repository;
namespace ProductApp.MVVCore.Services
{
    public class ProductService : IProductService
    {
        readonly IProductRepository _productRepository;
        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public int AddProduct(Product product)
        {
            Product? productDetailsExist = _productRepository.GetProductByName(product.Name);
            if(productDetailsExist == null)
            {
                return _productRepository.AddProduct(product);
            }
            else
            {
                throw new ProductExistException($"{product.Name} Already Exist!!");
            }
        }

        public int DeleteProduct(int id)
        {   //first cross check
            return _productRepository.DeleteProduct(id);
        }

        public Product? DeleteProductById(int id)
        {
            return _productRepository.DeleteProductById(id);
        }

        public Product? GetProductById(int id)
        {
            return _productRepository.GetProductById(id);
        }

        public List<Product> GetProducts()
        {
            return _productRepository.GetProducts();
        }
    }
}
